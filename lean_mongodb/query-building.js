const mongoose = require('mongoose')
const Room = require('./models/Room')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  // Update
//   const room = await Room.findById('6218b943b40e2de42ed55e30')
//   room.capacity = 20
//   room.save()
//   console.log(room)
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('------------------------------')
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(building))
}

main().then(() => {
  console.log('Finish')
})
